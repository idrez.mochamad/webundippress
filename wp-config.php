<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_estore_8000' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Pe,uXYeqd!:t#Mj:Dv`4PKxbO`Lq{XZv 6N>N3O6G2.J[VPS+5Nl]?d^u(R{Zn9B' );
define( 'SECURE_AUTH_KEY',  '.#dwo$IWNGX9UxWwC1-m.dpZRy&u-@2}vi4tnf_%(M]<ko1|ulvy#t$o,JV1f$~s' );
define( 'LOGGED_IN_KEY',    '~6~>-]<)a*9&pZoGL`ta^}l}%Ab~]9FmZ_l,WpJ^ FXL;vmmS^zoNmLP//a@zuSY' );
define( 'NONCE_KEY',        '=hK)i?>W[wnY7kgNq//CL9kl4=xFXdH?_VdBBK4H)!pm@U54(hD=ddsugH^h*aGZ' );
define( 'AUTH_SALT',        'IWO.i@{pu14iy*  70EmQB4v$X8i;<OMCX;(`f:1g8)4o8e4yE/q5iYreLH/CQsD' );
define( 'SECURE_AUTH_SALT', '9I=ytuNP5{F0Q^c}$?t2JDc 6`gRXu/t]~dz[@Q1jGrEafw5EtTO]^{| Bt:YP3C' );
define( 'LOGGED_IN_SALT',   'z]RW1*y*JZ8olrrkp#]W)hD-7tF{^Ok]DrSUJRlMI7t/fO>r}$X3X!I^57%)m.!9' );
define( 'NONCE_SALT',       'Z]v9*Ls1V02}*iklooH^+@NIYw])W~i%;(Tm8_%$]*GCbW)X+JCax6J!`$+4&Z{)' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
